#!/bin/sh
FILE=SystemaIosSDK.podspec
bump_version() {
	oldVersion=$(grep "version:" cz.yaml | awk '{print $2}')
	newVersion=$(cz bump --increment $1 --dry-run | grep tag | awk '{print $4}')
	echo "Changing ${oldVersion} -> ${newVersion}: ${FILE}"
  	sed -i '' "s/${oldVersion}/${newVersion}/" ${FILE};
        cz bump -ch --increment ${1}
}
