# ios-sdk-swift-xcframework

# Systema SDK Swift for iOS
This repository contains the `xcframework` artifact of [ iOS Systema SDK] ( https://bitbucket.org/systema-ai/systema-sdk-swift) for iOS written in swift.

It contains the API's for easy integration on an iOS mobile app.

##Installation
Below are few options to use this in your Podfile:

- Directly from the repository: master or develop branch

`pod 'SystemaIosSDK', :branch => 'master', :git => "https://bitbucket.org/systema-ai/systema-sdk-swift-xcframework"`

- Directly from the repository: specific tag 

`pod 'SystemaIosSDK', :tag => '0.1.0', :git => "https://bitbucket.org/systema-ai/systema-sdk-swift-xcframework"`

- From public Cocoapod spec: 

  `pod 'SystemaIosSDK', '~> 0.1.0'`
