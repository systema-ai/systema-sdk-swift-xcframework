## 0.1.1 (2022-04-13)

### Fix

- update with latest build

## 0.1.0 (2022-04-12)

### Fix

- fix URLs in podspec file
- add support for arm-64-simulator

## 0.0.0.2 (2022-03-10)

## 0.0.0.1 (2022-03-08)
