.PHONY: release-patch
release-patch:
	source bump.sh && bump_version PATCH

.PHONY: release-minor
release-minor:
	source bump.sh && bump_version MINOR

.PHONY: release-major
release-major:
	source bump.sh && bump_version MAJOR 
