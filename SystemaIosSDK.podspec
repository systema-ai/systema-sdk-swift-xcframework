Pod::Spec.new do |spec|
  spec.name         = "SystemaIosSDK"
  spec.version      = "0.1.4"
  spec.summary      = "Systema AI Mobile SDK - Swift iOS"
  spec.description  = <<-DESC
  Systema AI iOS Mobile SDK lets you easily integrate the Systema AI REST APIs with your iOS mobile apps.
    DESC
  spec.homepage     = "https://bitbucket.org/systema-ai/systema-sdk-swift-xcframework"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "The Systema AI Team" => "development@systema.ai" }
  spec.ios.deployment_target = "9.0"
  spec.source       = { :git =>"https://bitbucket.org/systema-ai/systema-sdk-swift-xcframework.git", :tag => "#{spec.version}" }
  spec.exclude_files = "Classes/Exclude"
  spec.vendored_frameworks      = "SystemaIosSDK.xcframework"
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
end
